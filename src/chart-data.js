export const crimeData = {
  "2007": [
    {
       "Month": 1,
       "Number": 100
    },
    {
       "Month": 2,
       "Number": 200
    },
    {
       "Month": 3,
       "Number": 300
    },
    {
       "Month": 4,
       "Number": 400
    }
 ],
 "2008": [
    {
       "Month": 7,
       "Number": 4000
    },
    {
       "Month": 8,
       "Number": 3000
    },
    {
       "Month": 9,
       "Number": 2000
    },
    {
       "Month": 10,
       "Number": 1000
    }
  ]
}
